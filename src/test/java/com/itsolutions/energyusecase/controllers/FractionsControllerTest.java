package com.itsolutions.energyusecase.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsolutions.energyusecase.SampleApplication;
import com.itsolutions.energyusecase.domain.Fraction;
import com.itsolutions.energyusecase.domain.FractionId;
import com.itsolutions.energyusecase.domain.Month;
import com.itsolutions.energyusecase.services.FractionsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleApplication.class)
@AutoConfigureMockMvc
public class FractionsControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private FractionsService fractionsService;

    @Test
    public void testGetFractions() throws Exception {

        Fraction fraction = new Fraction();
        fraction.setFraction(0.1);
        FractionId fractionId = new FractionId();
        fractionId.setMonth(Month.JAN);
        fractionId.setProfile("A");
        fraction.setFractionId(fractionId);

        Fraction fraction2 = new Fraction();
        fraction2.setFraction(0.2);
        FractionId fractionId2 = new FractionId();
        fractionId2.setMonth(Month.FEB);
        fractionId2.setProfile("A");
        fraction2.setFractionId(fractionId2);

        ArrayList<Fraction> allFractions = new ArrayList<>();
        allFractions.add(fraction);
        allFractions.add(fraction2);

        given(fractionsService.getAllFractions()).willReturn(allFractions);

        mvc.perform(get("/fractions"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].Month", is(String.valueOf(fraction.getFractionId().getMonth()))))
                .andExpect(jsonPath("$[0].Fraction", is(fraction.getFraction())))
                .andExpect(jsonPath("$[1].Month", is(String.valueOf(fraction2.getFractionId().getMonth()))))
                .andExpect(jsonPath("$[1].Fraction", is(fraction2.getFraction())));

        verify(fractionsService, VerificationModeFactory.times(1)).getAllFractions();
        reset(fractionsService);
    }

    @Test
    public void testCreateFraction() throws Exception {

        ArrayList<Fraction> fractionList = new ArrayList<>();
        Fraction fraction = new Fraction();
        fraction.setFraction(0.1);
        FractionId fractionId = new FractionId();
        fractionId.setMonth(Month.MAR);
        fractionId.setProfile("B");
        fraction.setFractionId(fractionId);
        fractionList.add(fraction);

        given(fractionsService.createFractions(fractionList)).willReturn(fractionList);
        given(fractionsService.validate(any())).willReturn(true);

        mvc.perform(post("/fractions")
                .content(asJsonString(fractionList))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        reset(fractionsService);
    }

    @Test
    public void testUpdateFraction() throws Exception {

        List<Fraction> fractionList = new ArrayList<>();
        Fraction fraction = new Fraction();
        fraction.setFraction(0.1);
        FractionId fractionId = new FractionId();
        fractionId.setMonth(Month.MAR);
        fractionId.setProfile("B");
        fraction.setFractionId(fractionId);
        fractionList.add(fraction);

        given(fractionsService.updateFraction(any())).willReturn(Optional.of(fraction));
        given(fractionsService.validate(any())).willReturn(true);

        mvc.perform(post("/fractions")
                .content(asJsonString(fractionList))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        fraction.setFraction(0.88);
        given(fractionsService.updateFraction(ArgumentMatchers.any())).willReturn(Optional.of(fraction));

        mvc.perform(put("/fractions")
                .content(asJsonString(fraction))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.Month", is(String.valueOf(fraction.getFractionId().getMonth()))))
                .andExpect(jsonPath("$.Fraction", is(fraction.getFraction())));

        reset(fractionsService);
    }

    @Test
    public void testDeleteFraction() throws Exception {

        Fraction fraction = new Fraction();
        fraction.setFraction(0.1);
        FractionId fractionId = new FractionId();
        fractionId.setMonth(Month.MAR);
        fractionId.setProfile("B");
        fraction.setFractionId(fractionId);

        given(fractionsService.deleteFraction(fractionId)).willReturn(Optional.of(fraction));

        mvc.perform(delete("/fractions")
                .content(asJsonString(fractionId))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        reset(fractionsService);
    }

    @Test
    public void testFindFraction() throws Exception {

        Fraction fraction = new Fraction();
        fraction.setFraction(0.1);
        FractionId fractionId = new FractionId();
        fractionId.setMonth(Month.MAR);
        fractionId.setProfile("B");
        fraction.setFractionId(fractionId);

        given(fractionsService.findFractionById(new FractionId(fraction.getFractionId().getMonth(), fraction.getFractionId().getProfile())))
                .willReturn(Optional.of(fraction));

        mvc.perform(get("/fractions/{month}/{profile}", Month.MAR, "B")
                .content(asJsonString(fraction))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.Month", is(String.valueOf(fraction.getFractionId().getMonth()))))
                .andExpect(jsonPath("$.Fraction", is(fraction.getFraction())));

        reset(fractionsService);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}