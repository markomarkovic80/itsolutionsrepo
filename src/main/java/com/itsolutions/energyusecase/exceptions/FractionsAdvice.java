package com.itsolutions.energyusecase.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class FractionsAdvice {

    @ResponseBody
    @ExceptionHandler(FractionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String fractionNotFoundHandler(FractionNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(FractionsDataValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String fractionsDataValidationFailedHandler(FractionsDataValidationException ex) {
        return ex.getMessage();
    }
}
