package com.itsolutions.energyusecase.controllers;

import com.itsolutions.energyusecase.SampleApplication;
import com.itsolutions.energyusecase.domain.FractionId;
import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.domain.MeterReadingId;
import com.itsolutions.energyusecase.domain.Month;
import com.itsolutions.energyusecase.services.MeterReadingsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.itsolutions.energyusecase.controllers.FractionsControllerTest.asJsonString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleApplication.class)
@AutoConfigureMockMvc
public class MeterReadingsTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MeterReadingsService meterReadingsService;

    @Test
    public void testGetMeterReadings() throws Exception {

        MeterReading meterReading = new MeterReading();
        meterReading.setMeterReading(23);
        MeterReadingId meterReadingId = new MeterReadingId();
        meterReadingId.setMeterId("0008");
        meterReadingId.setFractionId(new FractionId(Month.OCT, "B"));
        meterReading.setMeterReadingId(meterReadingId);

        MeterReading meterReading2 = new MeterReading();
        meterReading2.setMeterReading(23);
        MeterReadingId meterReadingId2 = new MeterReadingId();
        meterReadingId2.setMeterId("0009");
        meterReadingId2.setFractionId(new FractionId(Month.SEP, "A"));
        meterReading2.setMeterReadingId(meterReadingId2);

        List<MeterReading> meterReadings = Arrays.asList(meterReading, meterReading2);

        given(meterReadingsService.getAllMeterReadings()).willReturn(meterReadings);

        mvc.perform(get("/meterReadings").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].Month", is(String.valueOf(meterReading.getMeterReadingId().getFractionId().getMonth()))))
                .andExpect(jsonPath("$[0].['Meter reading']", is(meterReading.getMeterReading())))
                .andExpect(jsonPath("$[1].Month", is(String.valueOf(meterReading2.getMeterReadingId().getFractionId().getMonth()))))
                .andExpect(jsonPath("$[1].['Meter reading']", is(meterReading2.getMeterReading())));


        verify(meterReadingsService, VerificationModeFactory.times(1)).getAllMeterReadings();
        reset(meterReadingsService);
    }

    @Test
    public void testCreateMeterReading() throws Exception {

        List<MeterReading> meterReadingList = new ArrayList<>();
        MeterReading meterReading = new MeterReading();
        meterReading.setMeterReading(23);
        MeterReadingId meterReadingId = new MeterReadingId();
        meterReadingId.setMeterId("0008");
        meterReadingId.setFractionId(new FractionId(Month.OCT, "B"));
        meterReading.setMeterReadingId(meterReadingId);
        meterReadingList.add(meterReading);

        given(meterReadingsService.createMeterReading(any())).willReturn(meterReadingList);

        mvc.perform(post("/meterReadings")
                .content(asJsonString(meterReadingList))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$[0].MeterID", is("0008")))
                .andExpect(jsonPath("$[0].Month", is(String.valueOf(meterReading.getMeterReadingId().getFractionId().getMonth()))))
                .andExpect(jsonPath("$[0].['Meter reading']", is(meterReading.getMeterReading())));
        reset(meterReadingsService);
    }

    @Test
    public void testUpdateMeterReading() throws Exception {

        List<MeterReading> meterReadingList = new ArrayList<>();
        MeterReading meterReading = new MeterReading();
        meterReading.setMeterReading(23);
        MeterReadingId meterReadingId = new MeterReadingId();
        meterReadingId.setMeterId("0008");
        meterReadingId.setFractionId(new FractionId(Month.OCT, "B"));
        meterReading.setMeterReadingId(meterReadingId);
        meterReadingList.add(meterReading);

        given(meterReadingsService.createMeterReading(any())).willReturn(meterReadingList);

        mvc.perform(post("/meterReadings")
                .content(asJsonString(meterReadingList))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$[0].MeterID", is("0008")))
                .andExpect(jsonPath("$[0].Month", is(String.valueOf(meterReading.getMeterReadingId().getFractionId().getMonth()))))
                .andExpect(jsonPath("$[0].['Meter reading']", is(meterReading.getMeterReading())))
                .andExpect(jsonPath("$[0].Profile", is("B")));

        meterReading.setMeterReading(88);
        given(meterReadingsService.updateMeterReading(ArgumentMatchers.any())).willReturn(Optional.of(meterReading));
        mvc.perform(put("/meterReadings")
                .content(asJsonString(meterReading))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.MeterID", is("0008")))
                .andExpect(jsonPath("$.Month", is(String.valueOf(meterReading.getMeterReadingId().getFractionId().getMonth()))))
                .andExpect(jsonPath("$.['Meter reading']", is(meterReading.getMeterReading())))
                .andExpect(jsonPath("$.Profile", is("B")));

        reset(meterReadingsService);
    }

    @Test
    public void testDeleteMeterReading() throws Exception {

        MeterReading meterReading = new MeterReading();
        meterReading.setMeterReading(23);
        MeterReadingId meterReadingId = new MeterReadingId();
        meterReadingId.setMeterId("0008");
        meterReadingId.setFractionId(new FractionId(Month.OCT, "B"));
        meterReading.setMeterReadingId(meterReadingId);

        given(meterReadingsService.deleteMeterReading(meterReadingId)).willReturn(Optional.of(meterReading));

        mvc.perform(delete("/meterReadings")
                .content(asJsonString(meterReadingId))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        reset(meterReadingsService);
    }

    @Test
    public void testFindMeterReading() throws Exception {

        List<MeterReading> meterReadingList = new ArrayList<>();
        MeterReading meterReading = new MeterReading();
        meterReading.setMeterReading(23);
        MeterReadingId meterReadingId = new MeterReadingId();
        meterReadingId.setMeterId("0008");
        meterReadingId.setFractionId(new FractionId(Month.OCT, "B"));
        meterReading.setMeterReadingId(meterReadingId);
        meterReadingList.add(meterReading);

        given(meterReadingsService.createMeterReading(any())).willReturn(meterReadingList);
        given(meterReadingsService.findMeterReadingById(meterReadingId)).willReturn(Optional.of(meterReading));

        mvc.perform(get("/meterReadings/{meterId}/{month}/{profile}", "0008", Month.OCT, "B")
                .content(asJsonString(meterReading))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.Month", is(String.valueOf(meterReading.getMeterReadingId().getFractionId().getMonth()))))
                .andExpect(jsonPath("$.['Meter reading']", is(meterReading.getMeterReading())));

        reset(meterReadingsService);
    }

}
