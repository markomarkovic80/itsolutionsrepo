package com.itsolutions.energyusecase.services;

import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.domain.MeterReadingId;
import com.itsolutions.energyusecase.repositories.MeterReadingsRepository;
import com.itsolutions.energyusecase.validations.MeterReadingsConsecutiveMonthsReadingsValidation;
import com.itsolutions.energyusecase.validations.MeterReadingsProfileExistenceMeterReadingsValidation;
import com.itsolutions.energyusecase.validations.MeterReadingsValidationCondition;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MeterReadingsServiceImpl implements MeterReadingsService {

    private final MeterReadingsRepository meterReadingsRepository;
    private final FractionsService fractionsService;

    public MeterReadingsServiceImpl(MeterReadingsRepository meterReadingsRepository, FractionsService fractionsService) {
        this.meterReadingsRepository = meterReadingsRepository;
        this.fractionsService = fractionsService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<MeterReading> getAllMeterReadings() {
        return (ArrayList<MeterReading>) meterReadingsRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MeterReading> findMeterReadingById(MeterReadingId meterReadingId) {
        return meterReadingsRepository.findById(meterReadingId);
    }

    @Override
    public Iterable<MeterReading> createMeterReading(List<MeterReading> meterReadings) {
        return meterReadingsRepository.saveAll(meterReadings);
    }

    @Override
    public Optional<MeterReading> updateMeterReading(MeterReading meterReading) {
        Optional<MeterReading> existingMeterReading = meterReadingsRepository.findById(meterReading.getMeterReadingId());
        if (existingMeterReading.isPresent()) {
            existingMeterReading.get().setMeterReading(meterReading.getMeterReading());
            return Optional.of(meterReadingsRepository.save(meterReading));
        }
        return Optional.empty();
    }

    @Override
    public Optional<MeterReading> deleteMeterReading(MeterReadingId meterReadingId) {
        Optional<MeterReading> existingMeterReading = meterReadingsRepository.findById(meterReadingId);
        existingMeterReading.ifPresent(meterReading -> meterReadingsRepository.deleteById(meterReadingId));
        return existingMeterReading;
    }

    @Override
    public List<MeterReading> validate(List<MeterReading> meterReadings) {
        final List<MeterReadingsValidationCondition> validationsList = new ArrayList<>();
        validationsList.add(new MeterReadingsProfileExistenceMeterReadingsValidation(fractionsService));
        validationsList.add(new MeterReadingsConsecutiveMonthsReadingsValidation());

        List<MeterReading> validMeterReadings = new ArrayList<>();
        for (MeterReadingsValidationCondition validation : validationsList) {
            validMeterReadings = validation.validate(meterReadings);
            meterReadings = validMeterReadings;
        }
        return validMeterReadings;
    }
}
