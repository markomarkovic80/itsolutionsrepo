package com.itsolutions.energyusecase.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class FractionsDataValidationException extends RuntimeException {
    private String id;

    public FractionsDataValidationException(String id) {
        super(id);
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
