package com.itsolutions.energyusecase.controllers;

import com.itsolutions.energyusecase.SampleApplication;
import com.itsolutions.energyusecase.dao.ConsumptionDAO;
import com.itsolutions.energyusecase.domain.FractionId;
import com.itsolutions.energyusecase.domain.MeterReadingId;
import com.itsolutions.energyusecase.domain.Month;
import com.itsolutions.energyusecase.services.ConsumptionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleApplication.class)
@AutoConfigureMockMvc
public class ConsumptionControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ConsumptionService consumptionService;

    @Test
    public void testGetConsumption() throws Exception {

        MeterReadingId meterReadingId = new MeterReadingId();
        meterReadingId.setMeterId("0008");
        meterReadingId.setFractionId(new FractionId(Month.OCT, "B"));

        given(consumptionService.getConsumption(meterReadingId.getFractionId().getMonth(), meterReadingId.getMeterId()))
                .willReturn(Optional.of(new ConsumptionDAO(meterReadingId.getFractionId().getMonth(), meterReadingId.getMeterId(), 255)));
        mvc.perform(get("/consumption")
                .param("month", meterReadingId.getFractionId().getMonth().toString())
                .param("meterId", meterReadingId.getMeterId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.consumption", is(255)));
        reset(consumptionService);
    }
}
