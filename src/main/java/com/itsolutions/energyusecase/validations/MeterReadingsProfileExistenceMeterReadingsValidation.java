package com.itsolutions.energyusecase.validations;

import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.services.FractionsService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MeterReadingsProfileExistenceMeterReadingsValidation implements MeterReadingsValidationCondition {

    private final FractionsService fractionsService;

    @Autowired
    public MeterReadingsProfileExistenceMeterReadingsValidation(FractionsService fractionsService) {
        this.fractionsService = fractionsService;
    }

    @Override
    public List<MeterReading> validate(List<MeterReading> meterReadings) {
        Set<String> profileSet = new HashSet<String>();
        meterReadings.stream().forEach(mr -> profileSet.add(mr.getMeterReadingId().getFractionId().getProfile()));
        List<MeterReading> validMeterReadings = new ArrayList<>();

        profileSet.stream()
                .filter(p -> fractionsService.existFraction(p))
                .forEach(profile -> meterReadings.stream()
                        .filter(mr -> mr.getMeterReadingId().getFractionId().getProfile().compareTo(profile) == 0)
                        .forEach(meterReading -> validMeterReadings.add(meterReading)));

        return validMeterReadings;
    }
}
