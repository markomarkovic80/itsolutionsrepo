package com.itsolutions.energyusecase.domain;

public enum Month {
    JAN,
    FEB,
    MAR,
    APR,
    MAY,
    JUN,
    JUL,
    AUG,
    SEP,
    OCT,
    NOV,
    DEC;

    public Month getPreviousMonth() {
        return values()[ordinal() > 0 ? ordinal() - 1 : 0];
    }
}