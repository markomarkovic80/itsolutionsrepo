package com.itsolutions.energyusecase.repositories;

import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.domain.MeterReadingId;
import com.itsolutions.energyusecase.domain.Month;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MeterReadingsRepository extends CrudRepository<MeterReading, MeterReadingId> {

    @Query("SELECT mr FROM MeterReading mr WHERE mr.meterReadingId.fractionId.month = :month AND mr.meterReadingId.meterId = :meterId")
    Optional<MeterReading> findByMonthAndMeterId(@Param("month") Month month, @Param("meterId") String meterId);

    @Query("SELECT mr.meterReading from MeterReading mr WHERE mr.meterReadingId.meterId = :meterId")
    List<Integer> getMeterReadingsForMeterId(@Param("meterId") String meterId);
}
