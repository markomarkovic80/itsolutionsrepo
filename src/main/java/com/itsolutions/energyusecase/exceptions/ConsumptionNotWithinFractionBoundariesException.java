package com.itsolutions.energyusecase.exceptions;

public class ConsumptionNotWithinFractionBoundariesException extends RuntimeException {
    String id;

    public ConsumptionNotWithinFractionBoundariesException(String id) {
        super(String.format("Consumption for meterId-month combination: %s does not correspond to its' fraction boundaries", id));
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
