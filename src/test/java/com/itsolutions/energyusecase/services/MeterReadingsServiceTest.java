package com.itsolutions.energyusecase.services;

import com.itsolutions.energyusecase.EnergyConsumptionJpaConfig;
import com.itsolutions.energyusecase.domain.FractionId;
import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.domain.MeterReadingId;
import com.itsolutions.energyusecase.domain.Month;
import com.itsolutions.energyusecase.repositories.MeterReadingsRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {EnergyConsumptionJpaConfig.class})
public class MeterReadingsServiceTest {

    @Autowired
    private MeterReadingsService meterReadingsService;

    @MockBean
    private MeterReadingsRepository meterReadingsRepository;

    @Test
    public void findMeterReadingByIdSuccessfully() {
        MeterReading meterReading = getMeterReading();

        doReturn(Optional.of(meterReading)).when(meterReadingsRepository).findById(meterReading.getMeterReadingId());

        Optional<MeterReading> returnedMeterReading = meterReadingsService.findMeterReadingById(meterReading.getMeterReadingId());

        Assertions.assertThat(returnedMeterReading.isPresent());
        assertEquals(returnedMeterReading.get(), meterReading);
    }

    @Test
    public void testCreateMeterReadings() {
        MeterReading meterReading = getMeterReading();
        MeterReading meterReading2 = getMeterReading();
        List<MeterReading> meterReadingsList = Arrays.asList(meterReading, meterReading2);

        doReturn(meterReadingsList).when(meterReadingsRepository).saveAll(meterReadingsList);

        Iterable<MeterReading> meterReadingsCreated = meterReadingsService.createMeterReading(meterReadingsList);

        assertEquals(meterReadingsCreated, meterReadingsList);
    }

    @Test
    public void testUpdateMeterReading() {
        MeterReading meterReading = getMeterReading();
        MeterReading savedMeterReading = getMeterReading();
        savedMeterReading.setMeterReading(1604);

        doReturn(Optional.of(meterReading)).when(meterReadingsRepository).findById(meterReading.getMeterReadingId());
        doReturn(savedMeterReading).when(meterReadingsRepository).save(meterReading);

        Optional<MeterReading> updatedMeterReading = meterReadingsService.updateMeterReading(meterReading);

        assertEquals(savedMeterReading.getMeterReading(), updatedMeterReading.get().getMeterReading(), 0.0);
    }

    @Test
    public void testDeleteMeterReading() {
        MeterReading meterReading = getMeterReading();

        doReturn(Optional.of(meterReading)).when(meterReadingsRepository).findById(meterReading.getMeterReadingId());

        meterReadingsService.deleteMeterReading(meterReading.getMeterReadingId());

        verify(meterReadingsRepository, VerificationModeFactory.times(1)).deleteById(meterReading.getMeterReadingId());
    }

    private MeterReading getMeterReading() {
        MeterReading meterReading = new MeterReading();
        meterReading.setMeterReading(23);
        MeterReadingId meterReadingId = new MeterReadingId();
        meterReadingId.setMeterId("0008");
        FractionId fractionId = new FractionId(Month.OCT, "B");
        meterReadingId.setFractionId(fractionId);
        meterReading.setMeterReadingId(meterReadingId);

        return meterReading;
    }
}
