package com.itsolutions.energyusecase.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class MeterReadingsAdvice {

    @ResponseBody
    @ExceptionHandler(MeterReadingNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String meterReadingNotFoundHandler(MeterReadingNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(MeterReadingValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String meterReadingNotFoundHandler(MeterReadingValidationException ex) {
        return ex.getMessage();
    }
}
