package com.itsolutions.energyusecase.controllers;

import com.itsolutions.energyusecase.dao.ConsumptionDAO;
import com.itsolutions.energyusecase.domain.Month;
import com.itsolutions.energyusecase.exceptions.ConsumptionNotFoundException;
import com.itsolutions.energyusecase.services.ConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class ConsumptionController {

    private final ConsumptionService consumptionService;

    @Autowired
    public ConsumptionController(ConsumptionService consumptionService) {
        this.consumptionService = consumptionService;
    }

    @GetMapping(value = "/consumption", produces = {"application/json"})
    ResponseEntity<ConsumptionDAO> getConsumption(@RequestParam(value = "month") Month month, @RequestParam(value = "meterId") String meterId) {
        Optional<ConsumptionDAO> consumption = consumptionService.getConsumption(month, meterId);
        if (consumption.isPresent()) {
            return new ResponseEntity<>(consumption.get(), HttpStatus.OK);
        } else {
            throw new ConsumptionNotFoundException(meterId + "-" + month);
        }
    }
}
