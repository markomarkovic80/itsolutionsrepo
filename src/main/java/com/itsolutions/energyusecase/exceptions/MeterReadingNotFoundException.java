package com.itsolutions.energyusecase.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MeterReadingNotFoundException extends RuntimeException {

    private String id;

    public MeterReadingNotFoundException(String id) {
        super(String.format(" Meter reading for meterId-month-profile combination: '%s' not found", id));
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
