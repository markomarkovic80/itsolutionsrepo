package com.itsolutions.energyusecase.controllers;

import com.itsolutions.energyusecase.domain.Fraction;
import com.itsolutions.energyusecase.domain.FractionId;
import com.itsolutions.energyusecase.domain.Month;
import com.itsolutions.energyusecase.exceptions.FractionNotFoundException;
import com.itsolutions.energyusecase.exceptions.FractionsDataValidationException;
import com.itsolutions.energyusecase.services.FractionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class FractionsController {

    private final FractionsService fractionsService;

    @Autowired
    public FractionsController(FractionsService fractionsService) {
        this.fractionsService = fractionsService;
    }

    @GetMapping(value = "/fractions", produces = {"application/json"})
    ResponseEntity<List<Fraction>> getFractions() {
        List<Fraction> fractions = (ArrayList<Fraction>) fractionsService.getAllFractions();
        return new ResponseEntity<>(fractions, HttpStatus.OK);
    }

    @GetMapping(value = "/fractions/{month}/{profile}", produces = {"application/json"})
    Resource<Fraction> getFractionById(@PathVariable Month month, @PathVariable String profile) {
        Optional<Fraction> fractionById = fractionsService.findFractionById(new FractionId(month, profile));
        if (!fractionById.isPresent()) {
            throw new FractionNotFoundException(month + "-" + profile);
        }
        return new Resource<>(fractionById.get(),
                linkTo(methodOn(FractionsController.class).getFractionById(month, profile)).withSelfRel(),
                linkTo(methodOn(FractionsController.class).getFractions()).withRel("fractions"));
    }

    @PostMapping(value = "/fractions", consumes = {"application/json"}, produces = {"application/json"})
    ResponseEntity<List<Fraction>> createFractions(@RequestBody List<Fraction> fractions) {
        if (!fractionsService.validate(fractions)) {
            throw new FractionsDataValidationException("Fractions validation failed. At least one profile does not sum up to 1");
        }
        List<Fraction> createdFractions = (ArrayList<Fraction>) fractionsService.createFractions(fractions);
        return new ResponseEntity<>(createdFractions, HttpStatus.CREATED);
    }

    @PutMapping(value = "/fractions", consumes = {"application/json"}, produces = {"application/json"})
    Resource<Fraction> updateFraction(@RequestBody Fraction fraction) {
        Optional<Fraction> updatedFraction = fractionsService.updateFraction(fraction);
        if (!updatedFraction.isPresent()) {
            throw new FractionNotFoundException(fraction.getFractionId().getMonth() + "-" + fraction.getFractionId().getProfile());
        }
        if (!fractionsService.validate((ArrayList<Fraction>) fractionsService.getAllFractions())) {
            throw new FractionsDataValidationException("Fractions validation failed. At least one profile does not sum up to 1");
        }
        return new Resource<>(updatedFraction.get(),
                linkTo(methodOn(FractionsController.class).getFractionById(updatedFraction.get().getFractionId().getMonth(),
                        updatedFraction.get().getFractionId().getProfile())).withSelfRel(),
                linkTo(methodOn(FractionsController.class).getFractions()).withRel("fractions"));
    }

    @DeleteMapping(value = "/fractions")
    void deleteFraction(@RequestBody FractionId fractionId) {
        if (!fractionsService.deleteFraction(fractionId).isPresent()) {
            throw new FractionNotFoundException(fractionId.getMonth() + "-" + fractionId.getProfile());
        }
    }

}
