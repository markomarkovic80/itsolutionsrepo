package com.itsolutions.energyusecase.services;

import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.domain.MeterReadingId;

import java.util.List;
import java.util.Optional;

public interface MeterReadingsService {

    List<MeterReading> getAllMeterReadings();

    Iterable<MeterReading> createMeterReading(List<MeterReading> meterReadings);

    Optional<MeterReading> updateMeterReading(MeterReading meterReading);

    Optional<MeterReading> deleteMeterReading(MeterReadingId meterReadingId);

    Optional<MeterReading> findMeterReadingById(MeterReadingId meterReadingId);

    List<MeterReading> validate(List<MeterReading> meterReadings);

}
