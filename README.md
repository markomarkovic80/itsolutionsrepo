Energy Use Case Project


This project is a soulution to an Energy Use Case recruitment task from ITSoulutions.
It is developed as a Spring boot web application. 

Application is layered on:
  - Controllers that take user request via REST api
  - Services that process business logic
  - Repositories that handle database manipulation

Database which is used is H2 in-memory database

Spring boot version is 2.1.5, Java version is 1.8, all other dependices can be found in pom.xml file inside the project.

Maven is used for building, so "mvn package" commaned will build the project, run unit tests and create jar file for the solution.

REST api is documented via postman collection inside the project package in /src/main/resources folder
In that same folder, there are two files fractions.csv and meterreadings.csv which are used for initial population of database, and also as an example of appliaction solution of bonus task.
For easier testing purposes files are not deleted upon succesfull loading, but this can be changed by setting deleteCsvFiles property to true in application.properties file in resources folder.
Also, the files from which we read meter readings and fractions can be changed in application.properties.
Margin percent is by default set to 0.25 but it can be changed in application.properties file.
application.properites file contains path to log file and log level of springframework and hibarnate modules.

Unit tests are placed inside /src/test folder
