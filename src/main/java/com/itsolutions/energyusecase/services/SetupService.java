package com.itsolutions.energyusecase.services;

import com.itsolutions.energyusecase.domain.Fraction;
import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.repositories.FractionsRepository;
import com.itsolutions.energyusecase.repositories.MeterReadingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SetupService {

    private final FractionsRepository fractionsRepository;
    private final MeterReadingsRepository meterReadingsRepository;

    @Autowired
    public SetupService(FractionsRepository fractionsRepository, MeterReadingsRepository meterReadingsRepository) {
        this.fractionsRepository = fractionsRepository;
        this.meterReadingsRepository = meterReadingsRepository;
    }

    public void setupFraction(Fraction fraction) {
        if (!fractionsRepository.findById(fraction.getFractionId()).isPresent()) {
            fractionsRepository.save(fraction);
        }
    }

    public void setupMeterReading(MeterReading meterReading) {
        if (!meterReadingsRepository.findById(meterReading.getMeterReadingId()).isPresent()) {
            meterReadingsRepository.save(meterReading);
        }
    }

}
