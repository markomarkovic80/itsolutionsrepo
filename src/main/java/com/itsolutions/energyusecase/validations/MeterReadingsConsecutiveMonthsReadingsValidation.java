package com.itsolutions.energyusecase.validations;

import com.itsolutions.energyusecase.domain.MeterReading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MeterReadingsConsecutiveMonthsReadingsValidation implements MeterReadingsValidationCondition {
    @Override
    public List<MeterReading> validate(List<MeterReading> meterReadings) {
        Set<String> profileSet = new HashSet<>();
        meterReadings.stream().forEach(mr -> profileSet.add(mr.getMeterReadingId().getFractionId().getProfile()));

        List<MeterReading> validMeterReadings = new ArrayList<>();
        validMeterReadings.addAll(meterReadings);
        profileSet.stream()
            .forEach(p -> {
                List<MeterReading> meterReadingListByProfile = new ArrayList<>();
                meterReadings.stream()
                        .filter(meterReading -> meterReading.getMeterReadingId().getFractionId().getProfile().compareTo(p) == 0)
                    .forEach(mr -> meterReadingListByProfile.add(mr));
                sortMeterReadingListByMonth(meterReadingListByProfile);
                for (int i = 0; i < meterReadingListByProfile.size() - 1; i++) {
                    if (meterReadingListByProfile.get(i).getMeterReading() > meterReadingListByProfile.get(i + 1).getMeterReading()) {
                        validMeterReadings.removeIf(vmr -> vmr.getMeterReadingId().getFractionId().getProfile().compareTo(p) == 0);
                        break;
                    }
                }
            });

        return validMeterReadings;
    }

    private void sortMeterReadingListByMonth(List<MeterReading> meterReadings) {
        Collections.sort(meterReadings, (m1, m2) -> m1.getMeterReadingId().getFractionId().getMonth().ordinal() - m2.getMeterReadingId().getFractionId().getMonth().ordinal());
    }

}
