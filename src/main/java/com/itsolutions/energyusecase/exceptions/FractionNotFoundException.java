package com.itsolutions.energyusecase.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FractionNotFoundException extends RuntimeException {

    private String id;

    public FractionNotFoundException(String id) {
        super(String.format("Fraction for month-profile combination: '%s' not found", id));
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
