package com.itsolutions.energyusecase.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "meter_reading")
public class MeterReading {

    @EmbeddedId
    @JsonUnwrapped
    private MeterReadingId meterReadingId;

    @Column(name = "meter_reading")
    @JsonProperty("Meter reading")
    private int meterReading;

    public MeterReading() {
    }

    public MeterReading(MeterReadingId meterReadingId, int meterReading) {
        this.meterReadingId = meterReadingId;
        this.meterReading = meterReading;
    }

    public MeterReadingId getMeterReadingId() {
        return meterReadingId;
    }

    public void setMeterReadingId(MeterReadingId meterReadingId) {
        this.meterReadingId = meterReadingId;
    }

    public int getMeterReading() {
        return meterReading;
    }

    public void setMeterReading(int meterReading) {
        this.meterReading = meterReading;
    }

}