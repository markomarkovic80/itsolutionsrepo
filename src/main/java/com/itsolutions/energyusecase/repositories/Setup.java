package com.itsolutions.energyusecase.repositories;

import com.itsolutions.energyusecase.configuration.AppConfig;
import com.itsolutions.energyusecase.domain.Fraction;
import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.services.FractionsService;
import com.itsolutions.energyusecase.services.MeterReadingsService;
import com.itsolutions.energyusecase.services.SetupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class Setup {

    private static final Logger logger = LoggerFactory.getLogger(Setup.class);

    CsvDataLoader csvDataLoader = new CsvDataLoader();

    private final SetupService setupService;
    private final FractionsService fractionsService;
    private final MeterReadingsService meterReadingsService;
    private final AppConfig appConfig;

    @Autowired
    public Setup(SetupService setupService, FractionsService fractionsService, MeterReadingsService meterReadingsService, AppConfig appConfig) {
        this.setupService = setupService;
        this.fractionsService = fractionsService;
        this.meterReadingsService = meterReadingsService;
        this.appConfig = appConfig;
    }

    @PostConstruct
    private void setupData() {
        setupFractions();
        setupMeterReadings();
    }

    public List<Fraction> getFractions() {
        return csvDataLoader.loadObjectList(Fraction.class, appConfig.getProperty("fractionsFile"), appConfig.getProperty("deleteCsvFiles"));
    }

    public List<MeterReading> getMeterReadings() {
        return csvDataLoader.loadObjectList(MeterReading.class, appConfig.getProperty("meterReadingFile"), appConfig.getProperty("deleteCsvFiles"));
    }

    private void setupFractions() {
        if (fractionsService.validate(getFractions())) {
            getFractions().stream().forEach(fraction -> setupService.setupFraction(fraction));
            logger.info("Fractions successfully loaded from file");
        } else {
            logger.info("Fractions file is not valid for loading");
        }
    }

    private void setupMeterReadings() {
        List<MeterReading> validMeterReadings = meterReadingsService.validate(getMeterReadings());
        if (validMeterReadings.size() > 0) {
            validMeterReadings.stream().forEach(meterReading -> setupService.setupMeterReading(meterReading));
            logger.info("Meter readings successfully loaded from file");
        } else {
            logger.info("No meter readings from file were valid for loading.");
        }
    }

}