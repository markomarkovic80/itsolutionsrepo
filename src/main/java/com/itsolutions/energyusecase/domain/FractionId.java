package com.itsolutions.energyusecase.domain;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Embeddable
@JsonAutoDetect
public class FractionId implements Serializable {
    @NotNull
    @Size(max = 20)
    @JsonProperty("Month")
    @Column(name="month")
    private Month month;

    @NotNull
    @Size(max = 20)
    @JsonProperty("Profile")
    @Column(name="profile")
    private String profile;

    public FractionId() {

    }

    public FractionId(Month month, String profile) {
        this.month = month;
        this.profile = profile;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FractionId that = (FractionId) o;

        return month.equals(that.month) && profile.equals(that.profile);
    }

    @Override
    public int hashCode() {
        int result = month.hashCode();
        result = 31 * result + profile.hashCode();
        return result;
    }
}