package com.itsolutions.energyusecase.dao;

import com.itsolutions.energyusecase.domain.Month;

public class ConsumptionDAO {

    private Month month;
    private String meterId;
    private int consumption;

    public ConsumptionDAO() {

    }

    public ConsumptionDAO(Month month, String meterId, int consumption) {
        this.month = month;
        this.meterId = meterId;
        this.consumption = consumption;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public String getMeterId() {
        return meterId;
    }

    public void setMeterId(String meterId) {
        this.meterId = meterId;
    }

    public int getConsumption() {
        return consumption;
    }

    public void setConsumption(int consumption) {
        this.consumption = consumption;
    }


}
