package com.itsolutions.energyusecase.validations;

import com.itsolutions.energyusecase.domain.MeterReading;

import java.util.List;

@FunctionalInterface
public interface MeterReadingsValidationCondition {

    List<MeterReading> validate(List<MeterReading> meterReadings);
}

