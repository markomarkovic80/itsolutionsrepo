package com.itsolutions.energyusecase.controllers;

import com.itsolutions.energyusecase.domain.FractionId;
import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.domain.MeterReadingId;
import com.itsolutions.energyusecase.domain.Month;
import com.itsolutions.energyusecase.exceptions.MeterReadingNotFoundException;
import com.itsolutions.energyusecase.services.MeterReadingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class MeterReadingsController {

    private final MeterReadingsService meterReadingsService;

    @Autowired
    public MeterReadingsController(MeterReadingsService meterReadingsService) {
        this.meterReadingsService = meterReadingsService;
    }

    @GetMapping(value = "/meterReadings", produces = {"application/json"})
    ResponseEntity<List<MeterReading>> getMeterReadings() {
        List<MeterReading> meterReadings = meterReadingsService.getAllMeterReadings();
        return new ResponseEntity<>(meterReadings, HttpStatus.OK);
    }

    @GetMapping(value = "/meterReadings/{meterId}/{month}/{profile}", produces = {"application/json"})
    Resource<MeterReading> getMeterReadingById(@PathVariable String meterId, @PathVariable Month month, @PathVariable String profile) {
        Optional<MeterReading> meterReading = meterReadingsService.findMeterReadingById(new MeterReadingId(meterId, new FractionId(month, profile)));
        if (!meterReading.isPresent()) {
            throw new MeterReadingNotFoundException(meterId + "-" + month + "-" + profile);
        }
        return new Resource<>(meterReading.get(),
                linkTo(methodOn(MeterReadingsController.class).getMeterReadingById(meterId, month, profile)).withSelfRel(),
                linkTo(methodOn(MeterReadingsController.class).getMeterReadings()).withRel("meterReadings"));
    }

    @PutMapping(value = "/meterReadings", produces = {"application/json"})
    Resource<MeterReading> updateMeterReading(@RequestBody MeterReading meterReading) {
        Optional<MeterReading> updatedMeterReading = meterReadingsService.updateMeterReading(meterReading);
        if (!updatedMeterReading.isPresent()) {
            throw new MeterReadingNotFoundException(meterReading.toString());
        }

        return new Resource<>(updatedMeterReading.get(),
                linkTo(methodOn(MeterReadingsController.class).getMeterReadingById(updatedMeterReading.get().getMeterReadingId().getMeterId(),
                        updatedMeterReading.get().getMeterReadingId().getFractionId().getMonth(), updatedMeterReading.get().getMeterReadingId().getFractionId().getProfile())).withSelfRel(),
                linkTo(methodOn(MeterReadingsController.class).getMeterReadings()).withRel("meterReadings"))
                ;
    }

    @DeleteMapping(value = "/meterReadings")
    void deleteMeterReading(@RequestBody MeterReadingId meterReadingId) {
        if (!meterReadingsService.deleteMeterReading(meterReadingId).isPresent()) {
            throw new MeterReadingNotFoundException(meterReadingId.toString());
        }
    }

    @PostMapping(value = "/meterReadings", produces = {"application/json"})
    ResponseEntity<List<MeterReading>> createMeterReading(@RequestBody List<MeterReading> meterReadings) {
        List<MeterReading> createdMeterReadings = (ArrayList<MeterReading>) meterReadingsService.createMeterReading(meterReadingsService.validate(meterReadings));
        return new ResponseEntity<>(createdMeterReadings, HttpStatus.CREATED);
    }

}
