package com.itsolutions.energyusecase.repositories;

import com.itsolutions.energyusecase.domain.Fraction;
import com.itsolutions.energyusecase.domain.FractionId;
import com.itsolutions.energyusecase.domain.Month;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface FractionsRepository extends CrudRepository<Fraction, FractionId> {

    @Query("SELECT distinct count(f) FROM Fraction f WHERE f.fractionId.profile = :profile")
    int countProfilesByProfileName(@Param("profile") String profile);

    @Query("SELECT f.fraction FROM Fraction f WHERE f.fractionId.profile = :profile AND f.fractionId.month = :month")
    double getFraction(@Param("month") Month month, @Param("profile") String profile);
}