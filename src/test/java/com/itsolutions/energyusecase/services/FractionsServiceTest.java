package com.itsolutions.energyusecase.services;

import com.itsolutions.energyusecase.EnergyConsumptionJpaConfig;
import com.itsolutions.energyusecase.domain.Fraction;
import com.itsolutions.energyusecase.domain.FractionId;
import com.itsolutions.energyusecase.domain.Month;
import com.itsolutions.energyusecase.repositories.FractionsRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {EnergyConsumptionJpaConfig.class})
public class FractionsServiceTest {

    @Autowired
    private FractionsService fractionsService;

    @MockBean
    private FractionsRepository fractionsRepository;

    @Test
    public void testFindFractionByIdSuccess() {
        Fraction fraction = getFraction();

        doReturn(Optional.of(fraction)).when(fractionsRepository).findById(fraction.getFractionId());

        Optional<Fraction> returnedFraction = fractionsService.findFractionById(fraction.getFractionId());

        Assertions.assertThat(returnedFraction.isPresent());
        assertEquals(returnedFraction.get(), fraction);
    }

    @Test
    public void testFindFractionByIdNotFound() {
        Fraction fraction = getFraction();

        doReturn(Optional.empty()).when(fractionsRepository).findById(fraction.getFractionId());

        Optional<Fraction> returnedFraction = fractionsService.findFractionById(fraction.getFractionId());

        Assertions.assertThat(!returnedFraction.isPresent());
    }

    @Test
    public void testUpdateFraction() {
        Fraction fraction = getFraction();
        Fraction savedFraction = getFraction();
        savedFraction.setFraction(0.999);

        doReturn(Optional.of(fraction)).when(fractionsRepository).findById(fraction.getFractionId());
        doReturn(savedFraction).when(fractionsRepository).save(fraction);

        Optional<Fraction> updatedFraction = fractionsService.updateFraction(fraction);

        assertEquals(savedFraction.getFraction(), updatedFraction.get().getFraction(), 0.0);
    }

    @Test
    public void testDeleteFraction() {
        Fraction fraction = getFraction();

        doReturn(Optional.of(fraction)).when(fractionsRepository).findById(fraction.getFractionId());

        fractionsService.deleteFraction(fraction.getFractionId());

        verify(fractionsRepository, VerificationModeFactory.times(1)).deleteById(fraction.getFractionId());
    }

    private Fraction getFraction() {
        Fraction fraction = new Fraction();
        fraction.setFraction(0.888);
        FractionId fractionId = new FractionId();
        fractionId.setMonth(Month.AUG);
        fractionId.setProfile("A");
        fraction.setFractionId(fractionId);
        return fraction;
    }

}