package com.itsolutions.energyusecase.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ConsumptionNotFoundException extends RuntimeException {
    private String id;

    public ConsumptionNotFoundException(String id) {
        super(String.format("Consumption for meterId-month combination: '%s' not found", id));
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
