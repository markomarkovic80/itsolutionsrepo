package com.itsolutions.energyusecase.services;

import com.itsolutions.energyusecase.configuration.AppConfig;
import com.itsolutions.energyusecase.dao.ConsumptionDAO;
import com.itsolutions.energyusecase.domain.MeterReading;
import com.itsolutions.energyusecase.domain.Month;
import com.itsolutions.energyusecase.exceptions.ConsumptionNotFoundException;
import com.itsolutions.energyusecase.exceptions.ConsumptionNotWithinFractionBoundariesException;
import com.itsolutions.energyusecase.repositories.FractionsRepository;
import com.itsolutions.energyusecase.repositories.MeterReadingsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

@Service
public class ConsumptionServiceImpl implements ConsumptionService {

    private AppConfig appConfig;
    private final double marginPercent;
    private final MeterReadingsRepository meterReadingsRepository;
    private final FractionsRepository fractionsRepository;

    public ConsumptionServiceImpl(MeterReadingsRepository meterReadingsRepository, FractionsRepository fractionsRepository, AppConfig appConfig) {
        this.meterReadingsRepository = meterReadingsRepository;
        this.fractionsRepository = fractionsRepository;
        this.appConfig = appConfig;
        marginPercent =  Double.parseDouble(appConfig.getProperty("marginPercent"));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ConsumptionDAO> getConsumption(Month month, String meterId) {
        Optional<MeterReading> currentMonthMeterReading = meterReadingsRepository.findByMonthAndMeterId(month, meterId);
        Optional<MeterReading> previousMeterReading = meterReadingsRepository.findByMonthAndMeterId(month.getPreviousMonth(), meterId);
        if (!currentMonthMeterReading.isPresent() || !previousMeterReading.isPresent()) {
            return Optional.empty();
        }

        int consumption = currentMonthMeterReading.get().getMeterReading();
        if (!month.equals(Month.JAN)) {
            consumption -= previousMeterReading.get().getMeterReading();
        }

        if (validate(month, meterId, currentMonthMeterReading.get().getMeterReadingId().getFractionId().getProfile(), consumption)) {
            return Optional.of(new ConsumptionDAO(month, meterId, consumption));
        }

        throw new ConsumptionNotWithinFractionBoundariesException(meterId + "-" + month);
    }

    private boolean validate(Month month, String meterId, String profile, int consumption) {
        List<Integer> yearMeterReadings = meterReadingsRepository.getMeterReadingsForMeterId(meterId);
        Optional<MeterReading> meterReadingJanuary = meterReadingsRepository.findByMonthAndMeterId(Month.JAN, meterId);
        if (!meterReadingJanuary.isPresent()) {
            throw new ConsumptionNotFoundException(meterId + "-" + Month.JAN);
        }
        int yearConsumption = IntStream.range(0, yearMeterReadings.size() - 1)
                .map(i -> yearMeterReadings.get(i + 1) - yearMeterReadings.get(i))
                .reduce(0, Integer::sum)
                + meterReadingJanuary.get().getMeterReading();
        double fraction = fractionsRepository.getFraction(month, profile);
        return consumption <= (1 + marginPercent) * (yearConsumption * fraction) && consumption >= (1 - marginPercent) * (yearConsumption * fraction);
    }
}