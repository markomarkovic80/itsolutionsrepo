package com.itsolutions.energyusecase.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class ConsumptionsAdvice {

    @ResponseBody
    @ExceptionHandler(ConsumptionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String consumptionNotFoundHandler(ConsumptionNotFoundException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(ConsumptionNotWithinFractionBoundariesException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String consumptionNotWithinFractionBoundariesHandler(ConsumptionNotWithinFractionBoundariesException ex) {
        return ex.getMessage();
    }
}
