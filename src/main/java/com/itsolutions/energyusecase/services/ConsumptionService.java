package com.itsolutions.energyusecase.services;

import com.itsolutions.energyusecase.dao.ConsumptionDAO;
import com.itsolutions.energyusecase.domain.Month;

import java.util.Optional;

public interface ConsumptionService {

    Optional<ConsumptionDAO> getConsumption(Month month, String meterId);

}
