package com.itsolutions.energyusecase.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Embeddable
public class MeterReadingId implements Serializable {
    @NotNull
    @Size(max = 20)
    @JsonProperty("MeterID")
    private String meterId;

    @NotNull
    @JsonUnwrapped
    private FractionId fractionId;


    public MeterReadingId() {

    }

    public MeterReadingId(@NotNull @Size(max = 20) String meterId, @NotNull FractionId fractionId) {
        this.meterId = meterId;
        this.fractionId = fractionId;
    }

    public String getMeterId() {
        return meterId;
    }

    public void setMeterId(String meterId) {
        this.meterId = meterId;
    }

    public FractionId getFractionId() {
        return fractionId;
    }

    public void setFractionId(FractionId fractionId) {
        this.fractionId = fractionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MeterReadingId that = (MeterReadingId) o;

        if (meterId != null ? !meterId.equals(that.meterId) : that.meterId != null) return false;
        if (fractionId.getMonth() != null ? !fractionId.getMonth().equals(that.getFractionId().getMonth()) : that.getFractionId().getMonth() != null) return false;
        return fractionId.getProfile() != null ? fractionId.getProfile().equals(that.getFractionId().getProfile()) : that.getFractionId().getProfile() == null;
    }

    @Override
    public int hashCode() {
        int result = meterId != null ? meterId.hashCode() : 0;
        result = 31 * result + (fractionId !=null ? (fractionId.getMonth() != null ? fractionId.getMonth().hashCode() : 0) : 0)
                + (fractionId !=null ? (fractionId.getProfile() != null ? fractionId.getProfile().hashCode() : 0) : 0);
        return result;
    }

    @Override
    public String toString() {
        return meterId + "-" + fractionId.getMonth() + "-" + fractionId.getProfile();
    }
}