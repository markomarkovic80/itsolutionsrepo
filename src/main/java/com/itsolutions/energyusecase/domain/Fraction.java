package com.itsolutions.energyusecase.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "fraction")
public class Fraction {

    @EmbeddedId
    @JsonUnwrapped
    private FractionId fractionId;

    @Column(name = "fraction")
    @JsonProperty("Fraction")
    private double fraction;

    public Fraction() {
    }

    public Fraction(FractionId fractionId, long fraction) {
        this.fractionId = fractionId;
        this.fraction = fraction;
    }

    public FractionId getFractionId() {
        return fractionId;
    }

    public void setFractionId(FractionId fractionId) {
        this.fractionId = fractionId;
    }

    public double getFraction() {
        return fraction;
    }

    public void setFraction(double fraction) {
        this.fraction = fraction;
    }
}
