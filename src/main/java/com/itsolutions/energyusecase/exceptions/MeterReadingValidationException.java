package com.itsolutions.energyusecase.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MeterReadingValidationException extends RuntimeException {

    private String id;

    public MeterReadingValidationException(String id) {
        super(id);
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
