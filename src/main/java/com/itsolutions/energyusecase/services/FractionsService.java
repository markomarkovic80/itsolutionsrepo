package com.itsolutions.energyusecase.services;

import com.itsolutions.energyusecase.domain.Fraction;
import com.itsolutions.energyusecase.domain.FractionId;

import java.util.List;
import java.util.Optional;

public interface FractionsService {

    Iterable<Fraction> getAllFractions();

    Iterable<Fraction> createFractions(List<Fraction> fractions);

    Optional<Fraction> updateFraction(Fraction fraction);

    Optional<Fraction> deleteFraction(FractionId fractionId);

    Optional<Fraction> findFractionById(FractionId fractionId);

    boolean existFraction(String profile);

    boolean validate(List<Fraction> fractions);

}
