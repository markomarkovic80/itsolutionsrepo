package com.itsolutions.energyusecase.services;

import com.itsolutions.energyusecase.domain.Fraction;
import com.itsolutions.energyusecase.domain.FractionId;
import com.itsolutions.energyusecase.repositories.FractionsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class FractionsServiceImpl implements FractionsService {

    private final FractionsRepository fractionsRepository;

    public FractionsServiceImpl(FractionsRepository fractionsRepository) {
        this.fractionsRepository = fractionsRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Fraction> getAllFractions() {
        return fractionsRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Fraction> findFractionById(FractionId fractionId) {
        return fractionsRepository.findById(fractionId);
    }

    @Override
    public Iterable<Fraction> createFractions(List<Fraction> fractions) {
        return fractionsRepository.saveAll(fractions);
    }

    @Override
    public Optional<Fraction> updateFraction(Fraction fraction) {
        Optional<Fraction> existingFraction = fractionsRepository.findById(fraction.getFractionId());
        if (existingFraction.isPresent()) {
            existingFraction.get().setFraction(fraction.getFraction());
            return Optional.of(fractionsRepository.save(existingFraction.get()));
        }
        return Optional.empty();
    }

    @Override
    public Optional<Fraction> deleteFraction(FractionId fractionId) {
        Optional<Fraction> existingFraction = fractionsRepository.findById(fractionId);
        if (existingFraction.isPresent()) {
            fractionsRepository.deleteById(fractionId);
        }
        return existingFraction;
    }

    @Override
    public boolean existFraction(String profile) {
        return fractionsRepository.countProfilesByProfileName(profile) > 0;
    }

    @Override
    public boolean validate(List<Fraction> fractions) {
        Set<String> profileSet = new HashSet<>();
        fractions.stream().forEach(f -> profileSet.add(f.getFractionId().getProfile()));
        for (String profile : profileSet) {
            double sum = fractions.stream().filter(f -> f.getFractionId().getProfile().compareTo(profile) == 0).mapToDouble(Fraction::getFraction).sum();
            if (sum != 1.0) {
                return false;
            }
        }
        return true;
    }
}
